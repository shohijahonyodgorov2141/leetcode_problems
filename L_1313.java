public class L_1313 {
    public int[] decompressRLElist(int[] nums) {
        int len = 0;
        // {1,2,3,4}

        for(int i = 0; i<nums.length; i+=2){
            len+=nums[i];
        }// true


        int[] ans = new int[len];

        int index = 0;

        for(int i = 0; i<nums.length; i+=2){
            for(int j = index; j<index+nums[i]; j++){
                ans[j] = nums[i+1];
            }
            index += nums[i];

        }
        return ans;
    }
}
