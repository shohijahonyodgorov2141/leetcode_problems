import java.util.HashMap;
import java.util.Map;

public class ShuffleString_1528 {
    public String restoreString(String s, int[] indices) {
        Map<Integer, Character> map = new HashMap<>(s.length());
        StringBuilder natija = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            map.put(indices[i], s.charAt(i));
        }

        for (int i = 0; i < s.length(); i++) {
            Character a = map.get(i);
            natija.append(a);
        }
        return natija.toString();
    }
}
