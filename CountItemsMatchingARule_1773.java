import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class CountItemsMatchingARule_1773 {

    public int countMatches(List<List<String>> items, String ruleKey, String ruleValue) {

        Map<String, Integer> map  = new HashMap<>(3);
        map.put("type", 0);
        map.put("color", 1);
        map.put("name", 2);
        int count = 0;

        for (List<String> item:
             items) {
            if(Objects.equals(item.get(map.get(ruleKey)), ruleValue)){
                count++;
            }
        }
        return count;
    }
}
