import java.util.HashMap;
import java.util.Map;

public class TwoSum_1 {



    public int[] twoSum(int[] nums, int target) {
        int[] indexes = new int[2];


        Map<Integer, Integer> hashmap = new HashMap<>(nums.length);
        for (int i = 0; i < nums.length; i++) {
            Integer x = hashmap.get(target- nums[i]);
            if(x != null){
                return new int[]{x, i};
            }
            hashmap.put(nums[i],i);
        }
//        for (int i = 0; i < nums.length; i++) {
//            for (int j = 0; j < nums.length; j++) {
//                if(i == j){
//                    continue;
//                }
//                else {
//                    if(nums[i]+ nums[j]==target){
//                        indexes[0] = i;
//                        indexes[1] = j;
//                    }
//                }
//            }
//        }
        return new int[]{};
    }
}
