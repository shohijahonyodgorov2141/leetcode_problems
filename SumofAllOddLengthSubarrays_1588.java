public class SumofAllOddLengthSubarrays_1588 {

    public int sumArr(int[] a){
        int sum = 0;
        for (int i = 0; i < a.length; i++) {
            sum+=a[i];
        }
        return sum;
    }
    public int sumOddLengthSubarrays(int[] arr) {

        int sum = 0;
        int n = arr.length;

        for (int start = 0; start < n; start++) {
            for (int length = 1; start + length <= n; length += 2) { // Iterate through odd lengths
                int end = start + length - 1;
                for (int i = start; i <= end; i++) {
                    sum += arr[i];
                }
            }
        }

        return sum;
    }
}
