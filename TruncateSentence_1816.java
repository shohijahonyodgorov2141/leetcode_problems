public class TruncateSentence_1816 {
    public String truncateSentence(String s, int k) {
        String trimmed = s.trim();
        StringBuilder stringBuilder = new StringBuilder();

        String[] words = trimmed.split(" ");
        for (int i = 0; i < k; i++) {
            stringBuilder.append(words[i]).append(" ");

        }
        stringBuilder.delete(stringBuilder.length()-1, stringBuilder.length());
        return stringBuilder.toString();
    }
}
