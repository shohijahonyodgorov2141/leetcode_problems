public class MyNode <T> {

    T val;
    MyNode<T> next;

    MyNode(){

    }
    MyNode(T val){
        this.val = val;
        this.next = null;
    }

}
