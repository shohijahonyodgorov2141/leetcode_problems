public class NumberofArithmeticTriplets_2367 {

    public int arithmeticTriplets(int[] nums, int diff) {
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums.length; j++) {
                if(nums[i]-nums[j]==diff){
                    for (int k = 0; k < nums.length; k++) {
                        if(nums[k]-nums[i]==diff){
                            count++;
                        }
                    }
                }

            }
        }

        return count;
    }
}
