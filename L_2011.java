public class L_2011 {
    public static void main(String[] args) {
        L_2011 a  = new L_2011();
        System.out.println(a.finalValueAfterOperations(new String[]{"--X", "X--"}));

    }

    public int finalValueAfterOperations(String[] operations) {
        int X = 0;
        char a ='q';

        for(String operation:operations){
            if(operation.substring(0,2).equals("--") || operation.substring(1).equals("--")){
                X--;
            }
            else{
                X++;
            }
        }
        return X;
    }
}
